<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('source_id')->unique();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->bigInteger('feed_id')->unsigned()->index();
            $table->string('locale');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('excerpt')->nullable();
            $table->text('content')->nullable();
            $table->string('featured_image')->nullable();
            $table->string('featured_image_caption')->nullable();
            $table->integer('is_pro')->default('0');
            $table->string('type')->nullable();;
            $table->string("post_url")->nullable();
            $table->json('meta')->nullable();
            $table->enum('sticky', ['0', '1']);
            $table->enum('status', ['0', '1','2']);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('feed_id')->references('id')->on('feeds')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
