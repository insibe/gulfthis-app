<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unsigned();;
            $table->string('cr_no');
            $table->string('cr_en');
            $table->string('cr_ar');
            $table->string('slug');
            $table->string('cr_type');
            $table->string('cr_status');
            $table->longText('description')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone01')->nullable();
            $table->string('phone02')->nullable();
            $table->date('date_registered');
            $table->date('date_expired');
            $table->string('country');
            $table->enum('status', ['expired', 'active', 'disabled']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
