<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('locale');
            $table->string('title');
            $table->string('slug');
            $table->nestedSet();
            $table->string("type");
            $table->text('description')->nullable();
            $table->string('icon')->nullable();
            $table->string('cover')->nullable();
            $table->bigInteger('order')->nullable();
            $table->json('meta')->nullable();
            $table->enum('sticky', ['0', '1']);
            $table->enum('status', ['0', '1']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');

    }
}
