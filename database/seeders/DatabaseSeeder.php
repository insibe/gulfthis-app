<?php

namespace Database\Seeders;

use App\Models\Act;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\User::factory(1)->create();

        Act::factory()->times(50)->create();

//        \App\Models\Section::factory(160)->create();
//        \App\Models\SubSection::factory(20)->create();
    }
}
