<?php


use App\Models\Category;
use App\Models\Business;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Venturecraft\Revisionable\Revision;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $categoriesList =  Category::all();

    $posts =   \App\Models\Post::with('categories')->where('status','2')->where('locale','en')->orderBy('id', 'desc')->take(3)->get();
    $listings = \App\Models\Business::where('status','ACTIVE')->take(21)->get();



    return view('welcome',compact('categoriesList','posts','listings') );

});


Route::get('/list-of-companies-in-bahrain', function () {
    $listings = \App\Models\Business::where('status','ACTIVE')->paginate(10);
    return view('listings',compact('listings'));
});


Route::get('/listings/overview/{slug}', function ($slug) {
    $listing = \App\Models\Business::where('id', $slug)
        ->orWhere('slug', $slug)
        ->firstOrFail();
    return view('listings-single',compact('listing') );
});


Route::get('/news', function () {

    $posts = \App\Models\Post::where('status','2')->paginate(15);
    return view('news',compact('posts'));
});


Route::get('/news/{slug}', function ($slug) {

    $post = \App\Models\Post::where('id', $slug)
        ->orWhere('slug', $slug)
        ->firstOrFail();


    return view('post-single',compact('post') );
});


Route::get('/listings/category/{slug}', function ($slug) {

    $category = \App\Models\Category::where('id', $slug)
        ->orWhere('slug', $slug)
        ->firstOrFail();

   $listings =  $category->business()->orderBy('date_registered','desc')->paginate(10);



    return view('category-single',compact('listings','category') );
});





Route::get('/calendar', function () {

    $calendars = \App\Models\Calendar::orderby("order")->get();

    return view('calendars',compact('calendars'));
});

Route::get('/automobile', function () {

    $listings = \App\Models\Business::where('status','ACTIVE')->paginate(10);
    return view('automobile',compact('listings'));


});


Route::get('/banking-in-bahrain', function () {

    $listings = \App\Models\Business::where('status','ACTIVE')->paginate(10);
    return view('banking',compact('listings'));


});



Route::get('/search/', [\App\Http\Controllers\BusinessController::class, 'search'])->name('search');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    // Admin Dashboard
    Route::get('/dashboard', function () {
        return view('dashboard.index');
    });

    Route::get('/user/profile', function () {
        return view('user.profile');
    });
    Route::get('/user/profile/basic-info', function () {
        return view('user.basic-info');
    });

    Route::resource('dashboard/users', \App\Http\Controllers\UserController::class);
    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/listings', \App\Http\Controllers\BusinessController::class);
    Route::resource('dashboard/brands', \App\Http\Controllers\BusinessController::class);
    Route::resource('dashboard/feeds', \App\Http\Controllers\FeedController::class);
    Route::resource('dashboard/gold-rate', \App\Http\Controllers\FeedController::class);
    Route::get('dashboard/feeds/{id}/fetch', [\App\Http\Controllers\FeedController::class, 'fetch']);
    Route::resource('dashboard/posts', \App\Http\Controllers\PostController::class);
    Route::resource('dashboard/calendars', \App\Http\Controllers\CalendarController::class);
    Route::get('dashboard/settings/system-log', function () {

        $systemLogs = \Venturecraft\Revisionable\Revision::paginate('30');


        return view('dashboard.settings.system-log',compact('systemLogs'));
    });

});

Route::get('/settings', function () {

    $revisions_grouped = \Venturecraft\Revisionable\Revision::latest('id')->limit(500)->get()
        ->groupBy(function($revision) {
            return $revision->created_at->format('d.m.Y');
        });
    foreach($revisions_grouped as $date => $revisions) {
        echo '<strong>'.$date.'</strong><ul>';
        foreach($revisions as $revision) {
            echo '<li>'.$revision->created_at->format('h:i a').': modified <strong>'.$revision->key.'</strong> on '.
                class_basename($revision->revisionable_type).' '.$revision->revisionable_id.'</li>';
        }
        echo '</ul><hr />';
    }
    return view('dashboard.settings.index',compact('revisions_grouped'));

});


//Clear Cache facade value:
Route::get('/clear-cache', function() {

    return '<h1>Cache facade value cleared</h1>';
});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache facade value cleared</h1>';
});


Route::get('/migrate', function(){
    Artisan::call('migrate');
    return '<h1>MIGRATE</h1>';
});

Route::get('/guides', function () {
    return view('about-us' );
});



Route::get('/automotive', function () {
    return view('automotive' );
});

Route::get('company/about-us', function () {
    return view('about-us' );
});

Route::get('company/investor-relations', function () {
    return view('investor-relations' );
});

Route::get('company/editorial', function () {
    return view('editorial' );
});

Route::get('company/media-kit', function () {
    return view('media-kit' );
});

Route::get('company/careers', function () {
    return view('careers' );
});

Route::get('company/contact-us', function () {
    return view('contact-us' );
});

Route::get('legal/disclaimer', function () {
    return view('about-us' );
});

Route::get('terms-of-use', function () {
    return view('terms-of-use' );
});

Route::get('contact-us', function () {
    return view('about-us' );
});

Route::get('public-holidays-in-bahrain-2022', function () {

    $calendars= \App\Models\Calendar::where('status','0')->get();


    return view('public-holidays',compact('calendars'));
});
Route::get('bahrain-indemnity-calculator', function () {
    return view('indemnity-calculator' );
});

Route::get('/bahrain-metro-project', function () {
    return view('bahrain-metro' );
});

Route::get('/gold-price-bahrain-today', function () {
    return view('gold-rate' );
});

Route::get('generate-storage', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
});
Route::get('bahrain-national-day-2021', function () {
    return view('national-day' );
});
