<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use \Venturecraft\Revisionable\RevisionableTrait;
class Act extends Model
{
    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    use HasFactory;
    use HasSlug;
    use RevisionableTrait;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function categories()
    {
        return $this->belongsToMany(Category::class, 'act_categories');
    }


    public function chapters()
    {
        return $this->hasMany(Chapter::class);

    }
    public function acts(){

        return $this->belongsTo(Act::class);

    }



    public function sections()
    {
        return $this->hasManyThrough(Section::class,Chapter::class);

    }


    public function  subsections()
    {
        return $this->hasManyThrough(SubSection::class, Section::class);

    }




}
