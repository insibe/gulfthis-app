<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendars =  Calendar::paginate(15);
        $data = [
            'page_title' => 'Manage Calander'
        ];

        return view('dashboard.calendars.index',compact('calendars'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'calendar' => null,
            'formMethod' => 'POST',
            'mode' => 'CREATE',
            'url' => 'dashboard/calendars',
            'page_title' => 'Add a New Date'
        ];

        return view('dashboard.calendars.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $calendar = new Calendar();
            $calendar->locale                 = 'en';
            $calendar->title                  = $request->get('title');
            $calendar->description            = $request->get('description');
            $calendar->date                   = $request->get('date');
            $calendar->sticky                 = $request->get('sticky');
            $calendar->status                 = $request->get('status');
            $calendar->save();

            Alert::success('Success', 'New Calendar Added Successfully');
            return redirect('dashboard/calendars/'.$calendar->id.'/edit')->with('success', 'Calendar Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Calendar $calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calendar = Calendar::where('id', $id)->firstOrFail();


        $data = [
            'calendar' => $calendar,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => 'dashboard/calendars/'.$id,
            'page_title' => ' Edit '.$calendar->title
        ];

        return view('dashboard.calendars.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $calendar = Calendar::findOrFail($id);

            $calendar->locale                 = 'en';
            $calendar->title                  = $request->get('title');
            $calendar->description            = $request->get('description');
            $calendar->date                   = $request->get('date');
            $calendar->sticky                 = $request->get('sticky');
            $calendar->status                 = $request->get('status');

            $calendar->save();

            Alert::success('Success', 'Calendar Updated Successfully');
            return redirect('dashboard/calendars/'.$calendar->id.'/edit')->with('success', 'Calendar Updated Successfully!');
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calendar $calendar)
    {
        //
    }
}
