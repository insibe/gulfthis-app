<?php

namespace App\Http\Controllers;

use App\Models\GoldRate;
use Illuminate\Http\Request;

class GoldRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GoldRate  $goldRate
     * @return \Illuminate\Http\Response
     */
    public function show(GoldRate $goldRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GoldRate  $goldRate
     * @return \Illuminate\Http\Response
     */
    public function edit(GoldRate $goldRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GoldRate  $goldRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GoldRate $goldRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GoldRate  $goldRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoldRate $goldRate)
    {
        //
    }
}
