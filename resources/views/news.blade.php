@extends('layouts.theme')

@section('content')

    <!-- =======================
    Highlights END -->
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0"><i class="bi bi-newspaper me-2"></i>Latest News</h2>
                    <p>Checkout the hand pick post by admin</p>
                </div>
            @if(count($posts ) > 0)
                @foreach($posts as $post)

                    <!-- Card item START -->
                        <div class="col-sm-6 col-lg-4">
                            <div class="card mb-4">
                                <!-- Card img -->
                                <div class="card-fold position-relative">
                                    @if( $post->featured_image != null)
                                        <img class="card-img" src=" {{ \Storage::disk('public')->url($post->featured_image) ?? null }}" alt="Card image">
                                    @else
                                        <img class="card-img" src="{{ asset('theme/images/featured-placeholder.png') }} " alt="Card image">
                                    @endif
                                </div>
                                <div class="card-body px-0 pt-3">
                                    <h4 class="card-title"><a href="{{ url('/news/'.$post->slug ) }}" class="btn-link text-reset stretched-link fw-bold">{{ $post->title }}</a></h4>
                                    <!-- Card info -->
                                    <ul class="nav nav-divider align-items-center text-uppercase small">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link text-reset btn-link">Louis Ferguson</a>
                                        </li>
                                        <li class="nav-item">Mar 07, 2021</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Card item END -->

                    @endforeach
                @else

                    <a class="#">Nothing Found</a>

                @endif




            </div>
        </div>
    </section>
    <!-- =======================
    Tab post END -->

@endsection
