@extends('layouts.theme')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h2>Finance Tools, Calculators and Advice</h2>
                    <p>Use these free finance tools and make the best out of your hard earned money. Financial tips and calculators to help you plan your savings, investments, EMI and Taxes.</p>
                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h3>List of companies in Bahrain</h3>
                    <p>{{ $listings->count() }} unique companies found</p>
                    <div class="widget-box">
                        <ul>
                            <li><a href="#">Search for bank branches</a> </li>
                            <li><a href="#">Banks in Bahrain</a> </li>
                            <li><a href="#">Bahrain Indemnity Calculator</a> </li>
                            <li><a href="#">Bahrain bank holidays</a> </li>
                            <li><a href="#">Finance tips & calculators</a> </li>
                            <li><a href="#">Currency exchange rate calculator</a> </li>
                            <li><a href="#">Today's Gold Price in Bahrain</a> </li>
                            <li><a href="#">Today's Gasoline Price in Bahrain</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card-list">

                        <div itemscope="itemscope" itemtype="http://schema.org/ItemList" data-v-6c372401="">
                            <meta itemprop="numberOfItems" content="30" data-v-6c372401="">
                            <meta itemprop="name" content="List of companies in India" data-v-6c372401="">
                            <meta itemprop="url"
                                  content="https://www.ambitionbox.com/list-of-companies?utm_source=naukri&amp;utm_medium=gnb"
                                  data-v-6c372401="">
                            @if(count($listings ) > 0)
                                @foreach($listings as $listing)
                                    <div itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"
                                         class="company--card" data-v-6c372401="">
                                        <meta itemprop="position" content="1">
                                        <meta itemprop="name" content="TCS">
                                        <meta itemprop="alternateName" content="Tata Consultancy Services">
                                        <meta itemprop="url" content="https://www.ambitionbox.com/overview/tcs-overview">
                                        <meta itemprop="image"
                                              content="https://static.ambitionbox.com/alpha/company/photos/logos/tcs.jpg">
                                        <div class=" company-content-wrapper">
                                            <div class="company-content">
                                                <div class="company-logo">
                                                    <img src="{{ asset('theme/images/company-placeholder.svg') }}">
                                                </div>
                                                <div class="company-info-wrapper">
                                                    <div class="company-info">
                                                        <div class="left">
                                                            <a href="{{ url('/listings/overview/'.$listing->slug.'/') }}" class="company-titlephp "><h2 title="TCS" class="company-name">  {{ ucwords( strtolower($listing->cr_en) )   }}</h2></a>

                                                        </div>

                                                    </div>

                                                    <div class="company-basic-info"><p class="infoEntity sbold-list-header"><i
                                                                class="icon-domain"></i>
                                                        {{ $listing->cr_type }}
                                                        </p>
                                                        <p class="infoEntity sbold-list-header"><i class="icon-pin-drop"></i>
                                                            {{ $listing->country }}
                                                        </p>
                                                        <p class="infoEntity sbold-list-header"><i class="icon-access-time"></i>
                                                            53 years old
                                                        </p>
                                                        <p class="infoEntity sbold-list-header"><i
                                                                class="icon-supervisor-account"></i>
                                                            100001+ employees
                                                        </p></div>
                                                    <div class="chips-block">
                                                            @foreach($listing->categories as $category )
                                                            <a href="{{ url('/listings/category/'.$category->slug ) }}"
                                                               class="ab_chip body-medium"
                                                               data-filter-name="chips_Company-Tags_bpo-or-call-centre"
                                                               title="BPO / Call Centre companies in India">
                                                                {{ $category->title }}
                                                            </a>
                                                            @endforeach
                                                     </div>
                                                </div>
                                            </div>
                                            <p itemprop="description" class="description body-small">{{ $listing->description }}</p>
                                            <p itemprop="description" class="description body-small">{{ ucwords(strtolower($listing->cr_en))}} is a registered as a {{ $listing->cr_type }}  on {{ Carbon\Carbon::parse($listing->date_registered)->format('l jS F Y') }} in the Kingdom of Bahrain. {{ ucwords(strtolower($listing->cr_en))}}'s CR Number is {{ $listing->cr_no }}.{{ ucwords(strtolower($listing->cr_en))}} is in the industry of:
                                                <a href="{{ url('listings/category/'.$listing->categories->first()->slug) }}"> {{ $listing->categories->first()->title }}</a> and the company is based on Bahrain.</p>

                                            <p class="small"> Last updated on {{ Carbon\Carbon::parse($listing->updated_at)->format('jS F Y') }}</p>
                                        </div>

                                    </div>
                                @endforeach
                            @else

                            @endif




                        </div>

                       {{ $listings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container my-md-4 ">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <h3>Bahrain Business Directory </h3>
                            <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute
                                contact details and industry details. All the listings in this directory of business in
                                Bahrain are free and wont attract any kind of charges from business in Bahrain. All
                                business listed here will be reviewed from time to time. To add your business please
                                click on the green button below.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="category-list">
                                <ul>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-4">

            </div>
        </div>
    </div>

@endsection
