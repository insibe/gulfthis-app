@extends('layouts.theme')

@section('content')

    <section class="inner-hero banner_hero"
             style="background-image: url( {{ asset('theme/images/metro-hero.jpg') }} )" >

        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <div class="col-lg-7">
                        <h2 class="headline">Bahrain Metro Rail</h2>
                        <p>The Kingdom of Bahrain has announced the launch of a PPP
                            procurement for the new Bahrain Metro Project, an ambitious
                            project to develop a state-of-the-art transport network with a fully
                            automated, driverless system. S temming from Bahrain’s E conomic
                            Vision 2030, which focuses on the three pillars of sustainability,
                            competitiveness, and fairness, the proposed metro system will
                            further enhance accessibility and convenience for the Kingdom’s
                            residents and visitors.</p>
                    </div>
                    </div>
            </div>
        </div>
    </section>
    <!-- =======================
    Small post START -->
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2>In Focus – Bahrain Metro Phase One</h2>
                    <p>The overall project comprises 109km network with four transit lines
                        to be developed in phases. Bahrain Metro Phase One represents
                        the first phase of MTT’s ambitious plans and comprises two lines
                        with an estimated length of 28.6 km and includes 20 stations with
                        two interchanges. The two lines aim to connect the key transit
                        points including the airport as well as major residential, commercial
                        and, educational areas in Bahrain. </p>
                </div>


            </div>
        </div>
    </section>
    <!-- =======================
    Small post END -->

    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="popular_categories">
                    <div class="col-lg-12">
                        <div class="col-12 mb-3">
                            <h2 class="m-0"><i class="bi bi-hand-index-thumb me-2"></i>Latest News & Updates About Bahrain Metro</h2>
                            <p>Checkout the hand pick post by admin</p>
                        </div>


                    </div>
                    <div class="col-5">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">


                            <h2 class=" text-primary card-header-title">Newsletter</h2>



                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <form  action="{{ route('search') }}" method="GET">

                                <div class="input-group input-group-lg">
                                    <input class="form-control form-control-lg" name="search" placeholder="Your email address" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Subscribe</button> </span>
                                </div>
                                <!-- /input-group -->
                            </form>


                </div>
                <div class="col-lg-6">

                            <h2>Companies in Bahrain</h2>



                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <a href="{{ url('/') }}" class="btn btn-primary btn-lg  me-2">View All Businesses</a>

                </div>
            </div>
        </div>
    </section>
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0"><i class="bi bi-hand-index-thumb me-2"></i>Over 474k companies</h2>
                    <p>Get in depth information about companies. Know about their mission, vision, products, services and much more.</p>
                </div>

                <div class="col-lg-12">
                    <div id="companiesWrap">

                    </div>
                </div>


            </div>
        </div>
    </section>
@endsection
