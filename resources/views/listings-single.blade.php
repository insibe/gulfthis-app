@extends('layouts.theme')

@section('content')

    <section>
        <div class="container listing-single">
            <div class="row">
                <div class="col-lg-9">
                    <div class="col-lg-12">
                        <h3>  {{ ucwords(strtolower($listing->cr_en))   }}</h3>
                        @if (Auth::check())
                            <a href="{{ url('listings/'.$listing->id.'/edit') }}" class="btn btn-sm btn-outline-secondary">
                                Edit this Business
                            </a>
                        @else
                            <a href="#!" class="btn btn-sm btn-outline-secondary">
                                Claim this business
                            </a>
                        @endif
                        <p>{{ $listing->description }}</p>
                        <p>{{ ucwords(strtolower($listing->cr_en))}} is a registered as a {{ $listing->cr_type }}  on {{ Carbon\Carbon::parse($listing->date_registered)->format('l jS F Y') }} in the Kingdom of Bahrain. {{ ucwords(strtolower($listing->cr_en))}}'s CR Number is {{ $listing->cr_no }}.{{ ucwords(strtolower($listing->cr_en))}} is in the industry of:
                            <a href="{{ url('listings/category/'.$listing->categories->first()->slug) }}"> {{ $listing->categories->first()->title }}</a> and the company is based on Bahrain.</p>
                        <div class="chips-block">
                            @foreach($listing->categories as $category )
                                <a href="{{ url('/listings/category/'.$category->slug) }}"
                                   class="ab_chip body-medium"
                                   data-filter-name="chips_Company-Tags_bpo-or-call-centre"
                                   title="BPO / Call Centre companies in India">
                                    {{ $category->title }}
                                </a>
                            @endforeach
                        </div>


                        <h4 class="d-block">Company Information</h4>
                        <p> Find other contact information for {{ ucwords(strtolower($listing->cr_en))}} such as Email, Website and more below.</p>
                        <table class="table table-striped">
                            <thead>

                            </thead>
                            <tbody>
                            <tr>
                                <td scope="row">CR Number</td>
                                <td scope="row">{{ $listing->cr_no }}</td>
                            </tr>


                            <tr>
                                <td scope="row">Company Type</td>
                                <td scope="row">{{ $listing->cr_type }}</td>
                            </tr>
                            <tr>
                                <td scope="row">Country of Incorporation</td>
                                <td scope="row">{{ $listing->country }}</td>
                            </tr>
                            <tr>
                                <td scope="row">Date of Establishment</td>
                                <td colspan="2">  {{ Carbon\Carbon::parse($listing->date_registered)->format('j F Y') }} </td>

                            </tr>

                            <tr>
                                <td scope="row">Age of Company</td>
                                <td colspan="2">  {{ \Carbon\Carbon::parse($listing->date_registered)->diff(\Carbon\Carbon::now())->format('%y years, %m months and %d days')}}</td>


                            </tr>
                            </tbody>
                        </table>

                        <h5 class="d-block">Latest Jobs opening in {{ ucwords(strtolower($listing->cr_en))}} </h5>

                        <p>Currently, there are no vacancies in {{ ucwords(strtolower($listing->cr_en))}}</p>

                        <h5 class="card-header-title me-auto">
                            Frequently Asked Questions regarding {{ ucwords(strtolower($listing->cr_en)) }} ?
                        </h5>
                        <h6>Where are {{ ucwords(strtolower($listing->cr_en))}}   ’s Company Located?</h6>
                        <p>{{ ucwords(strtolower($listing->cr_en))}} company located in Kingdom of Bahrain</p>
                        <h6>When was the {{ ucwords(strtolower($listing->cr_en)) }} incorporated?</h6>
                        <p>The {{ ucwords(strtolower($listing->cr_en)) }} was incorporated with MOIC on  {{ Carbon\Carbon::parse($listing->date_registered)->format('j F Y') }} as {{ $listing->cr_type }}.</p>
                        <h6>What is {{ ucwords(strtolower($listing->cr_en))}}’s CR Number?</h6>
                        <p> {{ ucwords(strtolower($listing->cr_en))}}’s CR Number: {{ $listing->cr_no }}</p>
                        <h6> What is {{ ucwords(strtolower($listing->cr_en))}}s’s industry?</h6>
                        <p> {{ ucwords(strtolower($listing->cr_en))}} is in the industry of: {{ $listing->categories->first()->title }}</p>
                        <p class="small"> Last updated on {{ Carbon\Carbon::parse($listing->updated_at)->format('jS F Y') }}</p>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- gulfthis Square -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-2126121144705193"
                             data-ad-slot="2678859726"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>

                    <div class="card ">
                        <div class="card-body">
                            <h4> Everything you want to know about  {{ ucwords(strtolower($listing->cr_en))}} ? </h4>
                            <p>Join the 10,000+ businesses whose sales and marketing teams rely on Gulfthis's B2B database to identify, connect, and close their next customer.</p>
                            <p>With plans starting at just $29/month, sign up now to unlock access to:</p>
                        </div>
                    </div>


                    <div class="card ">
                        <div class="card-body">
                            <h4>Gulfthis App</h4>
                            <p> A smarter way to search for the local business in Bahrain.</p>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zeroPadding"> <img class=" img-responsive " data-qk-el-name="imglazyload" src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" data-original-src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" alt="Gulfthis mobile app" title="gulfthis mobile app"> </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
{{--    <div class="container my-md-4 ">--}}
{{--        <div class="row">--}}
{{--            <div class="col-8">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}
{{--                        <div class="col-12">--}}
{{--                            <h3>Bahrain Business Directory </h3>--}}
{{--                            <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute--}}
{{--                                contact details and industry details. All the listings in this directory of business in--}}
{{--                                Bahrain are free and wont attract any kind of charges from business in Bahrain. All--}}
{{--                                business listed here will be reviewed from time to time. To add your business please--}}
{{--                                click on the green button below.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}
{{--                        <div class="col-12">--}}
{{--                            <div class="category-list">--}}
{{--                                <ul>--}}

{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--            <div class="col-4">--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

@endsection
