@extends('layouts.guest-dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $category->title }} </h3>
                <div class="nk-block-des text-soft">
                    {{ $category->description }}


                </div>

            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="row g-gs">

            @if(count($category->descendants ) > 0)


                @foreach($category->descendants as $sub)
                    <a class=" btn btn-lg btn-outline-primary m-4"  href="#">{{ $sub->title }}</a>
                @endforeach
            @else
                @foreach($acts as $act)
                    <a class=" btn btn-lg btn-outline-primary m-4 " href="{{ url('/gst/'.$category->id.'/'.$act->slug)  }}">{{ $act->title }}</a>
                @endforeach
            @endif
        </div>
    </div><!-- .
@endsection
