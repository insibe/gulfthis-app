@extends('layouts.theme')

@section('content')


    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h3>Banks in Bahrain - Find Branches, IFSC, MICR Codes</h3>
                    <p>Use these free finance tools and make the best out of your hard earned money. Financial tips and calculators to help you plan your savings, investments, EMI and Taxes.</p>
                </div>
                <div class="col-lg-3">
                    <h3>Related Links</h3>
                    <div class="widget-box">
                        <ul>
                            <li><a href="#">Search for bank branches</a> </li>
                            <li><a href="#">Banks in Bahrain</a> </li>
                            <li><a href="#">Bahrain Indemnity Calculator</a> </li>
                            <li><a href="#">Bahrain bank holidays</a> </li>
                            <li><a href="#">Finance tips & calculators</a> </li>
                            <li><a href="#">Currency exchange rate calculator</a> </li>
                            <li><a href="#">Today's Gold Price in Bahrain</a> </li>
                            <li><a href="#">Today's Gasoline Price in Bahrain</a> </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="container my-md-4 ">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <h3>New Cars in the market </h3>
                            <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute
                                contact details and industry details. All the listings in this directory of business in
                                Bahrain are free and wont attract any kind of charges from business in Bahrain. All
                                business listed here will be reviewed from time to time. To add your business please
                                click on the green button below.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="category-list">
                                <ul>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-4">

            </div>
        </div>
    </div>

@endsection
