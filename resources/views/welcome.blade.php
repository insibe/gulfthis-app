@extends('layouts.theme')

@section('content')
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0">Editor’s pick</h2>
                    <p>Checkout the hand pick post by admin</p>
                </div>
                @if(count($posts ) > 0)
                    @foreach($posts as $post)
                            <div class="col-sm-6 col-lg-4">
                                <div class="card mb-4">

                                    <div class="card-fold position-relative">
                                        @if( $post->featured_image != null)
                                            <img class="card-img" src=" {{ \Storage::disk('public')->url($post->featured_image) ?? null }}" alt="Card image">
                                        @else
                                            <img class="card-img" src="{{ asset('theme/images/featured-placeholder.png') }} " alt="Card image">
                                        @endif
                                    </div>
                                    <div class="card-body px-0 pt-3">
                                        <h4 class="card-title"><a href="{{ url('/news/'.$post->slug ) }}" class="btn-link text-reset stretched-link fw-bold">{{ $post->title }}</a></h4>
                                        <!-- Card info -->
                                        <ul class="nav nav-divider align-items-center text-uppercase small">
                                            <li class="nav-item">
                                                <a href="#" class="nav-link text-reset btn-link">Louis Ferguson</a>
                                            </li>
                                            <li class="nav-item">Mar 07, 2021</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                @else
                    <a class="#">Nothing Found</a>
            @endif
            </div>
        </div>
    </section>
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="popular_categories">
                    <div class="col-lg-12">
                        <div class="col-12 mb-3">
                            <h2 class="m-0">Start Your Search With Our Most Popular Services</h2>
                            <p>From development to marketing, find your next business partner on Clutch.</p>
                        </div>

                            @if(count($categoriesList ) > 0)
                                @foreach($categoriesList as $category)

                                    <a href="{{ url('/listings/category/'.$category->slug ) }}"
                                       class="ab_chip body-medium"
                                       data-filter-name="chips_Company-Tags_bpo-or-call-centre"
                                       title="BPO / Call Centre companies in India">
                                        {{ $category->title }}
                                    </a>

                                @endforeach
                            @else

                                    <a class="#">Nothing Found</a>

                            @endif


                    </div>
                    <div class="col-5">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="widget-box">
                            <h2 class="card-header-title ">Newsletter</h2>
                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <form  action="{{ route('search') }}" method="GET">

                                <div class="input-group input-group-lg">
                                    <input class="form-control form-control-lg" name="search" placeholder="Your email address" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Subscribe</button> </span>
                                </div>
                            </form>
                    </div>
                </div>
                <div class="col-lg-6">
                          <div class="widget-box">
                              <h2>Discover Best Places to Work!</h2>
                              <p>The official Laravel job board connecting the best jobs with top talent.</p>
                              <a href="{{ url('/') }}" class="btn btn-primary btn-lg  me-2">Get Listed</a>
                          </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0">Over 474k companies</h2>
                    <p>Get in depth information about companies. Know about their mission, vision, products, services and much more.</p>
                </div>

                <div class="col-lg-12">
                    <div id="companiesWrap">
                    @if(count($listings ) > 0)
                        @foreach($listings as $listing)
                                <div class="company_item" ><a href="{{ url('/listings/overview/'.$listing->categories->first()->slug.'/'.$listing->slug ) }}" title="{{ $listing->title }}"><img class="company_logo" src="https://static.ambitionbox.com/alpha/company/photos/logos/tcs.jpg" onerror="this.onerror=null;this.src='https://static.ambitionbox.com/static/icons/company-placeholder.svg';" alt="{{ $listing->title  }}"></a></div>
                            @endforeach
                        @else
                            <a class="#">Nothing Found</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
