@extends('layouts.theme')

@section('content')
    <section class="inner-hero "
             style="" >
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1 >Media Kit</h1>
                    <p>People Who Power Our Existance</p>
                </div>
            </div>
        </div>
    </section>
    <!-- =======================
Main START -->
    <section>
        <div class="container position-relative" data-sticky-container>
            <div class="row">
                <!-- Main Content START -->
                <div class="col-lg-8 mb-5">

                    <div class="chips-block">
                        @foreach($post->categories as $category )
                            <a href="{{ url('/listings/category/'.$category->slug) }}"
                               class="ab_chip body-medium"
                               data-filter-name="chips_Company-Tags_bpo-or-call-centre"
                               title="BPO / Call Centre companies in India">
                                {{ $category->title }}
                            </a>
                        @endforeach
                    </div>

                    <span class="ms-2 small">Updated: {{ $post->updated_at }}</span>
                    <h1 class="post-title">{{ $post->title }}</h1>
                    <div class="post-meta">
                        <p>BY {{ $post->updated_at }}</p>
                    </div>
                    <div class="post-summery">

                    </div>
                    <div class="post-content">
                        {!! $post->content !!}
                    </div>


                    <!-- Divider -->
                    <div class="text-center h5 mb-4">. . .</div>

                    <h4>New Design and Innovative Display Technologies </h4>
                    <p>Saw bring firmament given hath gathering lights dry life rule heaven Give And fruit moving thing seed life day creepeth winged so divide him from day morning him open lesser male beginning him be bring evening life void fowl sixth morning that made is Was that his hath face light meat air female isn't over place replenish midst it of second grass good rule also in unto Called don't given waters Had creature Behold fly life from forth Moved night.</p>
                    <div class="row g-0">
                        <div class="col-sm-6 bg-primary-soft p-4 position-relative border-end border-1 rounded-start">
                            <span><i class="bi bi-arrow-left me-3 rtl-flip"></i>Previous post</span>
                            <h5 class="m-0"><a href="#" class="stretched-link btn-link text-reset">Dirty little secrets about the business industry</a></h5>
                        </div>
                        <div class="col-sm-6 bg-primary-soft p-4 position-relative text-sm-end rounded-end">
                            <span>Next post<i class="bi bi-arrow-right ms-3 rtl-flip"></i></span>
                            <h5 class="m-0"><a href="#" class="stretched-link btn-link text-reset">Bad habits that people in the industry need to quit</a></h5>
                        </div>
                    </div>
                </div>
                <!-- Main Content END -->
                <!-- Right sidebar START -->
                <div class="col-lg-4">
                    <div data-sticky data-margin-top="80" data-sticky-for="991">
                        <!-- About me -->
                        <div class="bg-light rounded p-3 p-md-4">
                            <div class="d-flex mb-3">
                                <!-- Avatar -->
                                <a class="flex-shrink-0" href="#">
                                    <div class="avatar avatar-xl border border-4 border-danger rounded-circle">
                                        <img class="avatar-img rounded-circle" src="assets/images/avatar/05.jpg" alt="avatar">
                                    </div>
                                </a>
                                <div class="flex-grow-1 ms-3">
                                    <span>Hello, I'm </span>
                                    <h3 class="mb-0">Louis Ferguson</h3>
                                    <p>An editor at Blogzine</p>
                                </div>
                            </div>
                            <p>Louis Ferguson is a senior editor for the blogzine and also reports on breaking news based in London. He has written about government, criminal justice, and the role of money in politics since 2015. </p>
                            <a href="#" class="btn btn-danger-soft btn-sm ">View articles</a>
                        </div>

                        <!-- Most read -->
                        <div>
                            <h5 class="mt-5 mb-3">Most read</h5>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">01</span>
                                <h6><a href="#" class="stretched-link">Bad habits that people in the business industry need to quit</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">03</span>
                                <h6><a href="#" class="stretched-link">How 10 worst business fails of all time could have been prevented</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">04</span>
                                <h6><a href="#" class="stretched-link">10 facts about business that will instantly put you in a good mood</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">05</span>
                                <h6><a href="#" class="stretched-link">How did we get here? The history of the business told through tweets</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">06</span>
                                <h6><a href="#" class="stretched-link">Ten tips about startups that you can't learn from books</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 fa-fw fw-bold fs-3 opacity-5">07</span>
                                <h6><a href="#" class="stretched-link">How to worst business fails of all time could have been prevented</a></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Right sidebar END -->
            </div>
        </div>
    </section>
    <!-- =======================
    Main END -->

@endsection
