
@extends('layouts.theme')
@section('content')
<div class="container my-md-4 ">
  <div class="row">
      <div class="col-8">
          <div class="card">
            <div class="card-header">
                <h3 class="card-header-title">Results for "v" </h3>
            </div>
              <div class="card-body">
                  <p>We found over  {{ $listings->count() }} relevant matches for your search term.</p>
                  <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute contact details and industry details. All the listings in this directory of business in Bahrain are free and wont attract any kind of charges from business in Bahrain. All business listed here will be reviewed from time to time. To add your business please click on the green button below.</p>
              </div>



          </div>


          @if(count($listings ) > 0)
              @foreach($listings as $listing)
                  <div itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"
                       class="company--card" data-v-6c372401="">
                      <meta itemprop="position" content="1">
                      <meta itemprop="name" content="TCS">
                      <meta itemprop="alternateName" content="Tata Consultancy Services">
                      <meta itemprop="url" content="https://www.ambitionbox.com/overview/tcs-overview">
                      <meta itemprop="image"
                            content="https://static.ambitionbox.com/alpha/company/photos/logos/tcs.jpg">
                      <div class=" company-content-wrapper">
                          <div class="company-content">
                              <div class="company-logo">
                                  <img src="{{ asset('theme/images/company-placeholder.svg') }}">
                              </div>
                              <div class="company-info-wrapper">
                                  <div class="company-info">
                                      <div class="left">
                                          <a href="{{ url('/listings/overview/'.$listing->slug.'/') }}" class=""><h2 title="TCS" class="company-name">  {{ ucwords( strtolower($listing->cr_en) )   }}</h2></a>

                                      </div>

                                  </div>

                                  <div class="company-basic-info"><p class="infoEntity sbold-list-header"><i
                                              class="icon-domain"></i>
                                          {{ $listing->cr_type }}
                                      </p>
                                      <p class="infoEntity sbold-list-header"><i class="icon-pin-drop"></i>
                                          {{ $listing->country }}
                                      </p>
                                      <p class="infoEntity sbold-list-header"><i class="icon-access-time"></i>
                                          53 years old
                                      </p>
                                      <p class="infoEntity sbold-list-header"><i
                                              class="icon-supervisor-account"></i>
                                          100001+ employees
                                      </p></div>
                                  <div class="chips-block">
                                      @foreach($listing->categories as $category )
                                          <a href="{{ url('/listing/category/'.$category->slug ) }}"
                                             class="ab_chip body-medium"
                                             data-filter-name="chips_Company-Tags_bpo-or-call-centre"
                                             title="BPO / Call Centre companies in India">
                                              {{ $category->title }}
                                          </a>
                                      @endforeach
                                  </div>
                              </div>
                          </div>
                          <p itemprop="description" class="description body-small">{{ $listing->description }}</p>
                          <p itemprop="description" class="description body-small">{{ ucwords(strtolower($listing->cr_en))}} is a registered as a {{ $listing->cr_type }}  on {{ Carbon\Carbon::parse($listing->date_registered)->format('l jS F Y') }} in the Kingdom of Bahrain. {{ ucwords(strtolower($listing->cr_en))}}'s CR Number is {{ $listing->cr_no }}.{{ ucwords(strtolower($listing->cr_en))}} is in the industry of:
                              <a href="{{ url('category/'.$listing->categories->first()->slug) }}"> {{ $listing->categories->first()->title }}</a> and the company is based on Bahrain.</p>

                          <p class="small"> Last updated on {{ Carbon\Carbon::parse($listing->updated_at)->format('jS F Y') }}</p>
                      </div>

                  </div>
              @endforeach
          @else

          @endif


          <div class="row">
              <div class="col-12">
                  <div class="col-12">
                     <div class="category-list">

                     </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-12">



              </div>
          </div>
      </div>
      <div class="col-4">

      </div>
  </div>
</div>


@endsection
