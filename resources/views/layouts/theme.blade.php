<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <script type="application/ld+json">
		{
		    "@context":"https://schema.org",
		    "@type":"WebSite",
		    "url":"https://www.gulfthis.com",
		    "name":"GulfThis",
		     "logo":{
		      "@context":"http://schema.org",
		      "@type":"ImageObject",
		      "url":"https://www.gulfthis.com/img/logo/logo-wide-light.png",
		      "height":77,
		      "width":512
		   },
		    "potentialAction": {
		        "@type": "SearchAction",
		        "target": https://gulfthis.com/search?search={search_term_string}",
		        "query-input": "required name=search_term_string"
		    },
		   "url":"https://www.gulfthis.com/",
		   "@id":"https://www.gulfthis.com/#publisher",
		   "sameAs":[
		      "https://www.facebook.com/GulfThis/",
		      "https://twitter.com/GulfThis",
		      "https://www.pinterest.com/GulfThis/",
		      "https://www.youtube.com/c/GulfThis"
		   ],

		   "alternateName":[
		      "GulfThis",
		      "GulfThis.com"
		   ]

		}
	</script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title','Gulfthis')</title>
    <meta name="description" content="@yield('meta_description','Gulfthis, Bahrain local search engine, Gulfthis curates Social content, News, Videos & more from Top Publishers on all Trending Topics.')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="https://gulfthis.com/" itemprop="url">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Gulfthis" />
    <meta property="og:description" content="Bahrain local search engine, Gulfthis curates Social content, News, Videos & more from Top Publishers on all Trending Topics" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:locale" content="en_US" />
    <link rel="canonical" href="{{url()->current()}}" />
    <link rel="home" href="{{url()->current()}}" />
    <meta name="google-site-verification" content="UOlx-Os2VCgYw_4GazZB1CeHRYomE-tLDFJVY2DSMPw" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('theme/images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('theme/images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/images/favicon-16x16.png') }}">
    <link rel="manifest" href="/site.webmanifest">
{{--    <link rel="stylesheet" href="{{ asset('theme/vendor/bootstrap-icons/bootstrap-icons.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('theme/css/style.css') }}">
    <link rel="dns-prefetch" type="text/css" href="{{ asset('theme/vendor/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/css/listings.css') }}">{{--
    <script src="https://kit.fontawesome.com/bac6a85ea1.js" crossorigin="anonymous"></script>
	<link rel="dns-prefetch" type="text/css" href="{{ asset('theme/vendor/tiny-slider/tiny-slider.css') }}">--}} {{--
	--}}

    <script async src="https://www.googletagmanager.com/gtag/js?id=G-DGTPWYP6GN"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-DGTPWYP6GN');
    </script>
</head>

<body>
<!-- =======================
Header START -->
<header class="py-3  border-bottom">
    <div class="container d-flex flex-wrap justify-content-center">
        <a href="{{ url('/') }}" class="d-flex align-items-center mb-3 mb-lg-0 me-lg-auto text-dark text-decoration-none">
            <img alt="Gulfthis logo" height="48" width="120" src="{{ asset('/theme/images/logo-gulfthis.svg') }}">
        </a>
        <form class="col-12 col-lg-auto mb-3 mb-lg-0" role="search">
            <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>
    </div>
</header>
<nav class="py-2 bg-light border-bottom">
    <div class="container d-flex flex-wrap">
        <ul class="nav me-auto">
            <li class="nav-item"><a href="{{ url('news') }}" class="nav-link link-dark px-2 active" aria-current="page">News</a></li>
            <li class="nav-item"><a href="{{ url('/automobile') }}" class="nav-link link-dark px-2">Cars</a></li>
            <li class="nav-item"><a href="{{ url('/list-of-companies-in-bahrain') }}" class="nav-link link-dark px-2">Companies</a></li>
            <li class="nav-item"><a href="{{ url('/banking-in-bahrain') }}" class="nav-link link-dark px-2">Banking</a></li>
            <li class="nav-item"><a href="#" class="nav-link link-dark px-2">About</a></li>
        </ul>
        <ul class="nav">
            <li class="nav-item"><a href="#" class="nav-link link-dark px-2">Login</a></li>
            <li class="nav-item"><a href="#" class="nav-link link-dark px-2">Sign up</a></li>
        </ul>
    </div>
</nav>

<main>@yield('content')</main>
<footer>
    <div class="container">
        <div class="row ">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>About Gulfthis</h5>
                <ul class="links">
                    <li><a href="{{ url('company/about-us') }}">Who we are</a>
                    </li>
                    <li><a href="{{ url('company/investor-relations') }}">Investor Relations</a>
                    </li>
                    <li><a href="{{ url('company/editorial') }}">Editorial Policy</a>
                    </li>
                    <li><a href="{{ url('company/media-kit')}}">Media kit</a>
                    </li>
                    <li><a href="{{ url('company/careers') }}">Careers</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Resources</h5>
                <ul class="links">
                    <li><a href="{{ url('/public-holidays-in-bahrain-2022') }}">Public Holidays in Bahrain</a>
                    </li>
                    <li><a href="{{ url('/bahrain-metro-project') }}">Bahrain Metro Project</a>
                    </li>
                    <li><a href="{{ url('/bahrain-indemnity-calculator') }}">Bahrain Indemnity Calculator</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>For Businesses</h5>
                <ul class="links">
                    <li><a href="{{ url('/bahrain-national-day-2021') }}">Bahrain National Day 2021 </a>
                    </li>
                    <li><a href="{{ url('/gold-price-bahrain-today') }}">Today Gold Rate </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Others </h5>
                <ul class="links">
                    <li><a href="{{ url('/news') }}">News</a>
                    </li>
                    <li><a href="{{ url('/cars') }}">Cars</a>
                    </li>
                    <li><a href="{{ url('/companies') }}">Companies</a>
                    </li>
                    <li><a href="{{ url('/banking') }}">Banking</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /row-->
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p class="copy-right">By continuing past this page, you agree to our <a href="{{ url('terms-of-use') }}">Terms of use</a>,<a href="{{ url('cookie-policy') }}"> Cookie Policy</a>, <a href="{{ url('editorial-policy') }}"> Editorial Policy</a>,<a href="{{ url('privacy-policy') }}"> Privacy Policy</a> and <a href="{{ url('content-policy') }}"> Content Policies</a>. All trademarks are properties of their respective owners. © 2020. <a href="https://gulfthis.com/">Gulfthis - Bahrain's No.1
                        Local Search Engine</a> . All rights reserved.</p>
            </div>
        </div>
        <hr>
        <div class="footer-info">
            <div class="row">
                <div class="col-12">
                    <h4>Have news to share?</h4>
                    <p>Email us at <a href="mail:editorial@gulfthis.com">editorial@gulfthis.com</a> , or send us your company press releases to <a href="mail:pressrelease@gulfthis.com">pressrelease@gulfthis.com</a>
                    </p>
                    <h4>Gulfthis - Bahrain's No.1 Local Search Engine </h4>
                    <p class="copy-right">Gulfthis.com is Bahrain's No. 1 Local Search engine that provides local search related services to users across Bahrain through multiple platforms such as website, mobile website, Apps (Android, iOS). GulfThis.com is a one of its kind Hyper Local portal, which lets you know the whole spectrum of what’s happening in the Kingdom of Bahrain.</p>
                    <h4>Some of our services that will prove useful to you on a day-to-day basis are:</h4>
                    <div class="row">
                        <div class="col-lg-4">
                            <h6>Gulfthis - Shorts News</h6>
                            <p class="copy-right">Gulfthis Instant news is a news app that selects latest and best news from multiple national and international sources and summarises them to present in a short & image format, personalized for you.</p>
                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Automotive Guide in Bahrain </h6>
                            <p class="copy-right">A platform where car buyers and owners can research, buy, sell and come together to discuss and talk about their cars.</p>
                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Business </h6>
                            <p class="copy-right">Search for anything in Bahrain, anywhere you are with GulfThis Business app.GulfThis Business app now gives you phone numbers, maps, diections and more about every business in Bahrain, all on the go.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="back-top"><i class="bi bi-arrow-up-short"></i></div>

<script src="{{ asset('theme/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/js/functions.js') }}"></script>
</body>

</html>
