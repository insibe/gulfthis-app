<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Zayati: New Cars, Car Prices, Automotive Guide, Buy & Sell Used Cars in Bahrain.</title>
    <meta name="description" content="Zayati is Bahrain's most authentic source of new car pricing and other cars related information. Zayati exists to simplify car buying in Bahrain and helps you buy the right car at the right price.">
    <meta name="author" content="Insibe Business Solutions">
    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }} " type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/apple-touch-icon-57x57.png') }} ">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/apple-touch-icon-60x60.png') }} ">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/apple-touch-icon-114x114.png') }} ">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('img//favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('img/android-chrome-192x192.png') }}" sizes="192x192">
    <meta name="msapplication-square70x70logo" content="{{ asset('img/smalltile.png') }}" />
    <meta name="msapplication-square150x150logo" content="{{ asset('img/mediumtile.png') }}" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('img/widetile.png') }}" />
    <meta name="msapplication-square310x310logo" content="{{ asset('img/largetile.png') }}" />

    <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/vendors.css') }}" rel="stylesheet">
    {{--    <link href="css/style.css" rel="stylesheet">--}}
    {{--    <link href="css/vendors.css" rel="stylesheet">--}}

    {{--    <!-- YOUR CUSTOM CSS -->--}}





</head>

<body>

<div id="page">

    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light ">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}"><img height="48" src="{{ asset('/theme/images/logo-gulfthis.svg') }}"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample05">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('news') }}">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/automobile') }}">Cars</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('listings') }}" tabindex="-1" >Businesses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/banking') }}">Banking</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('community') }}" tabindex="-1" >Community</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-md-0">
                        <input class="form-control" type="text" placeholder="Search">
                    </form>
                </div>
            </div>
        </nav>

    </header>
    <!-- /header -->

    <main class="pattern">

        @yield('content')


    </main>
    <!-- /main -->

    <footer>
        <div class="container">

            <div class="row ">
                <div class="col-lg-12 pb-5">
                    <div class="logo_footer">
                        <img src="{{ asset('/theme/images/logo-gulfthis.svg') }}" alt="Zomato logo" height="40px" >
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5 >About Zayati</h5>
                    <ul class="links">
                        <li><a href="{{ url('company/about-us') }}">Who we are</a></li>
                        <li><a href="{{ url('company/investor-relations') }}">Investor Relations</a></li>
                        <li><a href="{{ url('company/editorial') }}">Editorial Policy</a></li>
                        <li><a href="{{ url('company/media-kit')}}">Media kit</a></li>
                        <li><a href="{{ url('company/careers') }}">Careers</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>Tools</h5>
                    <ul class="links">
                        <li><a href="{{ url('tools/emi-calculator') }}">EMI Calculator</a></li>
                        <li><a href="{{ url('tools/car-recommender') }}">Car Recommender</a></li>
                        <li><a href="{{ url('tools/used-car-valuation') }}">Used Car Valuation</a></li>



                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>For Businesses</h5>
                    <ul class="links">
                        <li><a href="{{ url('listing/add-listing') }}">List your Business</a></li>
                        <li><a href="{{ url('listing/claim-listing') }}">Claim your Listings</a></li>
                        <li><a href="{{ url('/dealer-solutions') }}">Business Solutions</a></li>
                        <li><a href="{{ url('/advertise-with-us') }}">Advertise with us</a></li>

                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>Others </h5>
                    <ul class="links">
                        <li><a href="#0">Disclaimer</a></li>
                        <li><a href="#0">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <!-- /row-->

            <div class="row pt-5">
                <div class="col-lg-12">
                    <p class="copy-right">By continuing past this page, you agree to our <a href="{{ url('terms-and-conditions') }}">Terms of Service</a>,<a href="{{ url('cookie-policy') }}"> Cookie Policy</a>, <a href="{{ url('editorial-policy') }}"> Editorial Policy</a>,<a href="{{ url('privacy-policy') }}"> Privacy Policy</a> and <a href="{{ url('content-policy') }}">  Content Policies</a>. All trademarks are properties of their respective owners. © 2020 - <a href="#">Zayati - Bahrain's Leading Automotive Guide</a> . All rights reserved.</p>
                </div>
            </div>

            <hr>
            <div class="footer-info">
                <div class="row">
                    <div class="col-12">
                        <h6>About Zayati - Bahrain's Leading Automotive Guide</h6>
                        <p class="copy-right">Zayati.com is Middleeast's leading car search venture that helps users buy cars that are right for them. Zayati has always striven to serve car buyers and owners in the most comprehensive and convenient way possible. We provide a platform where car buyers and owners can research, buy, sell and come together to discuss and talk about their cars.</p>
                        <h6>Our Mission</h6>
                        <p class="copy-right">Our mission is to bring joy and delight into car buying and ownership. To achieve this goal, we aim to empower Indian consumers to make informed car buying and ownership decisions with exhaustive and un-biased information on cars through our expert reviews, owner reviews, detailed specifications and comparisons. We understand that a car is by and large the second-most expensive asset a consumer associates his lifestyle with.</p>
                        <h6> How did Zayati get named?</h6>
                        <p class="copy-right"> After endless debate over several cups of coffee, we came up with the name Zayati. We decided to keep the idea of My Car at the center and choose a name that is short, easy to remember and makes people think of Car. Zayati's got a zing to it and is originally a play on the word 'Sayarti',Translated from Arabic word meaning 'My Car'.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
</div>
<!-- page -->


<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="js/select2.min.js"></script>
<script src="{{ asset('/js/owl.carousel.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();

        $('.owl-carousel').owlCarousel({
            items:5,
            lazyLoad:true,
            loop:true,
            nav:false,
            margin:10
        });
    });
</script>

</body>
</html>
