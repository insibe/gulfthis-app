@extends('layouts.dashboard')

@section('css_before')
    <link rel="stylesheet" href="{{ asset('backend/css/editors/tinymce.css') }}">


@endsection

@section('js_after')
    <script src="{{ asset('backend/js/libs/editors/tinymce.js') }}"></script>
    <script src="{{ asset('backend/js/editors.js') }}"></script>

    <script>


    </script>
@endsection



@section('content')
    {!! Form::model($business, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    {{ $business }}
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em
                                class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">


                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">
        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">
                        <div class="form-group">
                            <label class="form-label"> Title <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('cr_en',null, ['class' => 'form-control', 'placeholder'=>'Enter Post Title..','required' =>'required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label"> Content <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('cr_type',null, ['data-parsley-errors-container' => '#content-errors','class' => 'form-control  tinymce-toolbar','row' => 6,'data-parsley-trigger'=>'keyup','required' =>'required']) !!}
                            </div>
                            <div id="content-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="phone-no"> Excerpt / Summery</label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('excerpt',null, ['class' => 'form-control','rows' => 1]) !!}
                            </div>
                            <div id="counter"></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"> Post URL / Post Source </label>
                            <div class="form-control-wrap">
                                {!! Form::text('post_url',null, ['class' => 'form-control', 'placeholder'=>'Enter Post Source URL..']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label"> Meta Title </label>
                            <div class="form-control-wrap">
                                {!! Form::text('meta_title',null, ['class' => 'form-control', 'placeholder'=>'Enter Meta Title..']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"> Meta Description</label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('meta_description',null, ['rows' => 2,'class' => 'form-control', 'placeholder'=>'Enter Meta Description']) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-bordered bg-lighter ">
                    <div class="card-inner">
                        <div class="form-group">
                            <label class="form-label"> Language <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('locale' ,[''=>'','ml'=>'Malayalam','en'=>'English'],null, ['data-parsley-errors-container' => '#language-errors','data-placeholder' => 'Select Language','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                <div id="language-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Author<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('user_id',$userList ,null, ['data-parsley-errors-container' => '#author-errors','data-placeholder' => 'Select Author','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                <div id="author-errors"></div>
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="form-label">Sticky <span>*</span></label>
                            <div class="form-control-wrap">

                                {!! Form::select('sticky',[''=>'','0'=>'No Sticky','1'=>'Sticky'] ,null, ['data-parsley-errors-container' => '#sticky-errors','data-placeholder' => 'Select Post  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="sticky-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="form-label">Status <span>*</span></label>
                            <div class="form-control-wrap">

                                {!! Form::select('status',[''=>'','0'=>'Draft','1'=>'Scheduled','2'=>'Published'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Post  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save </button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/categories/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div><!-- .nk-block -->
    {!! Form::close() !!}




@endsection
