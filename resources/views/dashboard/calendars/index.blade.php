@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $calendars->count() }} Calendars.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/calendars/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Date</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Title</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Subitle</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Act</span></div>

            </div><!-- .nk-tb-item -->
            @if(count($calendars ) > 0)
                @foreach($calendars as $calendar)
                    <div class="nk-tb-item">


                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/calendars/'.$calendar->id.'/edit') }}">
                                <span class="tb-lead">   {{ $calendar->title }}  </span>
                            </a>
                        </div>

                        <div class="nk-tb-col">

                                <span class="tb-lead">   {{ $calendar->subtitle }} </span>

                        </div>
                        <div class="nk-tb-col">

                            <span class="tb-lead"> {{ $calendar->title }} </span>

                        </div>


                        <div class="nk-tb-col nk-tb-col-tools">

                            <a href="{{ url('dashboard/feeds/'.$calendar->id.'/fetch') }}" class="btn btn-sm btn-outline-primary">Fetch</a>
                        </div>
                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $calendars->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection
