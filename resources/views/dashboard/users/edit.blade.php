@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-10">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($act ?? '', array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label">Title <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('title',null, ['class' => 'form-control ', 'placeholder'=>'Category Title','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Description</label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Feed Description..','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >With Effect From<span>*</span></label>
                            <div class="form-control-wrap">
                                <div class="form-icon form-icon-left">
                                    <em class="icon ni ni-calendar"></em>
                                </div>
                                {!! Form::text('wef',null, ['class' => 'form-control date-picker ', 'placeholder'=>'Category Title','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Act Category<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('categories[]', $categories ,null, ['data-parsley-errors-container' => '#category-errors','data-placeholder' => 'Select Primary Category','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                            </div>
                            <div id="status-parent"></div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Order</label>
                            <div class="form-control-wrap">
                                {!! Form::number('order',null, ['class' => 'form-control', 'placeholder'=>'Act Order','required' =>'required']) !!}

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Act Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Feed Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                        <div class="form-group">

                            <button type="submit" class="float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'edit')

                            <form action="{{url('dashboard/acts/'.$act->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Act</button>
                            </form>
                        @else

                        @endif


                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
