@extends('layouts.theme')

@section('content')

    <section class="inner-hero banner_hero"
             style="background-image: url( {{ asset('theme/images/national-day-hero.jpg') }} )" >

        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                   <div class="col-lg-7">
                       <h2 class="headline">Bahrain National Day 2021</h2>
                       <p> Bahrain on December 16 and 17 celebrates its National Days, in commemoration of the establishment of the Bahraini state as an Arab and Muslim country founded by Ahmed Al Fateh in 1783, the anniversary of its full membership in the United Nations, and the anniversary of His Majesty the King's Accession to the Throne.</p>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =======================
    Small post START -->
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0"><i class="bi bi-newspaper me-2"></i></h2>
                    <p>As per the previous studies, Bahrain Metro network needs was identified in a total length of 109km to be developed on phases. The length of  phase  one is  29Km and it will include 20 stations which will be operate through two lines, where the first line will be connecting Airport to Seef District , while the other line will connect Juffair to educational area in Isa Town.</p>
                </div>


            </div>
        </div>
    </section>
    <!-- =======================
    Small post END -->

    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="popular_categories">
                    <div class="col-lg-12">
                        <div class="col-12 mb-3">
                            <h2 class="m-0"><i class="bi bi-hand-index-thumb me-2"></i>Latest News & Updatest</h2>
                            <p>Checkout the hand pick post by admin</p>
                        </div>


                    </div>
                    <div class="col-5">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">


                            <h2 class=" text-primary card-header-title">Newsletter</h2>



                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <form  action="{{ route('search') }}" method="GET">

                                <div class="input-group input-group-lg">
                                    <input class="form-control form-control-lg" name="search" placeholder="Your email address" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Subscribe</button> </span>
                                </div>
                                <!-- /input-group -->
                            </form>


                </div>
                <div class="col-lg-6">

                            <h2>Companies in Bahrain</h2>



                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <a href="{{ url('/') }}" class="btn btn-primary btn-lg  me-2">View All Businesses</a>

                </div>
            </div>
        </div>
    </section>
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="m-0"><i class="bi bi-hand-index-thumb me-2"></i>Over 474k companies</h2>
                    <p>Get in depth information about companies. Know about their mission, vision, products, services and much more.</p>
                </div>

                <div class="col-lg-12">
                    <div id="companiesWrap">

                    </div>
                </div>


            </div>
        </div>
    </section>
@endsection
