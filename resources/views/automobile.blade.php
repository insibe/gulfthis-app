@extends('layouts.theme')

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h2 class="headline">Let's find your perfect car in Bahrain</h2>
                    <p>Compare offers on new & nearly new cars, from top rated dealers in Bahrain</p>
                    <div class="col-md-12 col-md-offset-3">
                        <form  action="{{ route('search') }}" method="GET">

                            <div class="input-group input-group-lg">
                                <input class="form-control form-control-lg" name="search" placeholder="Search for anything, anywhere in Bahrain" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Go</button> </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>

                    <h3>New Cars in the market</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>

                    <h3>Find Your Car</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>


                    <h3>Recently Launched Cars</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>

                    <h3>Upcoming Models</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>

                    <h3>Latest Automobile News</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>


                    <h3>Browse Cars by Company</h3>
                    <p>Click on a logo to find details about latest cars, models and variants offered by the company along with price and specifications.</p>
                </div>
                <div class="col-lg-3">
                    <h3>Related links</h3>
                   <div class="widget-box">
                       <ul>
                           <li><a href="#">Car Affordability Calculator</a> </li>
                           <li><a href="#">Car dealer search</a> </li>
                           <li><a href="#">Car loan EMI</a> </li>
                           <li><a href="#">Car Insurance Calculator</a> </li>
                       </ul>
                   </div>

                </div>
            </div>
        </div>
    </section>
    <div class="container my-md-4 ">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <h3>New Cars in the market </h3>
                            <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute
                                contact details and industry details. All the listings in this directory of business in
                                Bahrain are free and wont attract any kind of charges from business in Bahrain. All
                                business listed here will be reviewed from time to time. To add your business please
                                click on the green button below.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="category-list">
                                <ul>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-4">

            </div>
        </div>
    </div>

@endsection
