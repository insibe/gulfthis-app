@extends('layouts.theme')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h2>Bahrain Public & National Holidays (2022)</h2>
                    <p> Consist mostly of Islamic Holidays and are based on the lunar calendar subject to moon sighting.
                        Therefore, the dates of these Islamic holidays vary from year to year.</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-9">
                    <p>Bahrain is a country in the GCC region which consists of more than 30 islands. It covers an area of about 765.3 square kilometers (295.5 sq mi). Back in the 19th century, Bahrain’s pearl fisheries were considered the best in the world. Bahrain is home to a population of 1.425 million including locals and expatriates. Bahrain is the third smallest nation in Asia after Maldives and Singapore. However small the size of the country, Bahrain has a high Human Development Index and belongs to the high-income economies of the world, according to the World Bank.</p>

                    <div class="row">
                        <div class="col-lg-12 ">
                            <h4> When are public holidays in Bahrain in 2022?</h4>
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Weekday</th>
                                    <th>Holiday name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($calendars ) > 0)
                                    @foreach($calendars as $calendar)
                                <tr>
                                    <td>{{ $calendar->title }}</td>
                                    <td>{{ $calendar->date }}</td>
                                    <td>{{ $calendar->description }}</td>
                                </tr>

                                    @endforeach
                                @else

                                @endif
                                </tbody>
                            </table>


                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="small">Muslim festivals are timed according to local sightings of various phases of the moon, and the dates given above are approximations. Therefore, the dates of the holidays are subject to change, and we cannot guarantee their complete accuracy. Also, the number of days of holiday is based on expected days off for the private sector.</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 ">

                </div>
            </div>


            <div class="row">

            </div>


        </div>
    </section>
@endsection
